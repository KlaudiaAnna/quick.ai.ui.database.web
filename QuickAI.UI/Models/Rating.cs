﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace QuickAI.UI.Models
{
    public class Rating
    {
        public int HumanRating { get; set; }
        public int NeuralNetwork { get; set; }
        public string Notes { get; set; }
    }
}