﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
namespace QuickAI.UI.Models
{
    public class Tweet
    {
        [BsonId] public ObjectId Id { get; set; }
        [BsonElement("Date")] public DateTime Date { get; set; }
        [BsonElement("Symbol")] public string Symbol { get; set; }
        [BsonElement("Id_Tweet")] public Int64 Id_Tweet { get; set; }
        [BsonElement("Text")] public string Text { get; set; }
        [BsonElement("ID_User")] public Int64 ID_User { get; set; }
        [BsonElement("ID_Name")] public string User_Name { get; set; }
        [BsonElement("Portal")] public string Portal { get; set; }
        [BsonElement("Human_rating")] public string Human_rating { get; set; }
        [BsonElement("Neural_rating")] public string Neural_rating { get; set; }
        [BsonElement("Date_Inserted")] public DateTime Date_Inserted { get; set; }
    }
}