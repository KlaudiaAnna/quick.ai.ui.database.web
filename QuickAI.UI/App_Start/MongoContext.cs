﻿using MongoDB.Driver;
using QuickAI.UI.Models;
namespace QuickAI.UI
{
    public class MongoContext
    {
        public IMongoDatabase DatabaseBase;
        public IMongoCollection<Tweet> Collection;
        public MongoContext()
        {
            var client = new MongoClient("mongodb://127.0.0.1:27017");
            DatabaseBase = client.GetDatabase("Tickersdata");
            Collection = DatabaseBase.GetCollection<Tweet>("TweetV1");
        }
    }
}