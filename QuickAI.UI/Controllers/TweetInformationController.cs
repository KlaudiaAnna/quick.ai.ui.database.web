﻿using System.Linq;
using System.Web.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using QuickAI.UI.Models;
using  PagedList;
using System;
using System.Globalization;
namespace QuickAI.UI.Controllers
{
    public class TweetInformationController : Controller
    {
        public MongoContext MongoContext;

        public TweetInformationController()
        {
            MongoContext = new MongoContext();
        }

        [HttpGet]
        public ActionResult Index(int? page)
        {
            var documents = MongoContext.Collection.Find(new BsonDocument()).ToList();

            ViewBag.DateToday = DateTime.Now.ToLongDateString();
            return View(documents.ToList().ToPagedList(page ?? 1, documents.Count));
        }

        [HttpGet]
        public ActionResult Delete(string id)
        {
            MongoContext.Collection.DeleteOne(Builders<Tweet>.Filter.Eq("_id", ObjectId.Parse(id)));
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Insert(string Human_rating, string id)
        {
            var filter = (Builders<Tweet>.Filter.Eq("_id", ObjectId.Parse(id)));

            MongoContext.Collection.UpdateOne(
                filter,
                Builders<Tweet>.Update
                    .Set("Human_rating", Human_rating));

            return Content("Inserted to Database");
        }

        [HttpGet]
        public ActionResult Filter(string from, string to, int? page)
        {
            const string format_date = "T00:00:00.000Z";
            /*returned index if both date are null */
            if ((from == "" && to == "") || (to != "" && from == ""))
            {
                return RedirectToAction("Index");
            }

            if (to == "" && from != null)
            {
                DateTime d_from;
                string link = from + format_date;
                if (DateTime.TryParseExact(link, "yyyy-MM-ddTHH:mm:ss.fffZ", CultureInfo.InvariantCulture,
                    DateTimeStyles.None, out d_from))
                {
                    //    var filtered_to = MongoContext.Collection.Find(x => x.Date_Inserted == d_from).ToList();
                    var query = MongoContext.Collection.AsQueryable().Where(x => x.Date_Inserted == d_from);
                    return View("Index", query.ToPagedList(page ?? 1, query.Count()));
                }
                else
                {
                    return Content("Cannot format this Date.");
                }
            }
            else
            {
                string link = from + format_date;
                DateTime d_from, d_to;
                string link_ = to + format_date;

                if (DateTime.TryParseExact(link, "yyyy-MM-ddTHH:mm:ss.fffZ", CultureInfo.InvariantCulture,
                    DateTimeStyles.None, out d_from))
                {
                    if (DateTime.TryParseExact(link_, "yyyy-MM-ddTHH:mm:ss.fffZ", CultureInfo.InvariantCulture,
                        DateTimeStyles.None, out d_to))
                    {
                        var filtered_date = MongoContext.Collection.AsQueryable()
                            .Where(x => x.Date_Inserted >= d_from && x.Date_Inserted <= d_to);
                        return View("Index", filtered_date.ToPagedList(page ?? 1, filtered_date.Count()));
                    }
                    else
                    {
                        var filtered_to = MongoContext.Collection.AsQueryable().Where(x => x.Date_Inserted == d_from)
                            .ToList();
                        return View("Index", filtered_to.ToPagedList(page ?? 1, filtered_to.Count));
                    }
                }
                else return Content("Cannot format this Date.");
            }

        }
        [HttpGet]
        public ActionResult Refresh()
        {
            return RedirectToAction("Index");
        }
    }
}